import React from "react";
import {AiFillHeart} from 'react-icons/ai';
import {BiDollar} from 'react-icons/bi';


const CardItemOne = ({data}) => {
  return (
    <>
      <div className="card">
          <h5 className="card-title mt-2 text-center"><span className=" text-dark p-1 ">{data.name}</span></h5>
        <img
          className="card-img-top mt-2"
          src={data.photo}
          alt="Card image cap"
        />
        <div className="card-body">
          <p className="card-text">
            {
              data.description
            }
          </p>
          <div className="row mb-1">
          <span className="col-md-1"></span>
          <span className="col-md-3 rate-room ">{data.rate} <i><AiFillHeart /></i></span>
          <span className="col-md-3"></span>
              <span className="col-md-3 rate-room-new">{data.price}<i><BiDollar size={20}/></i></span>
          </div>
          <div className="bg-dark text-center">

          <a href="#" className="btn text-white">
            Detail
          </a>
          </div>
        </div>
      </div>
    </>
  );
};

export default CardItemOne;