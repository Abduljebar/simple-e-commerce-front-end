import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Home from "./components";
import ProductList from "./components/product_list";
import ProductDetail from "./components/product_detail";

function App() {
  return (
    <Router>
      <div className="">
        <ul className="">
          {/* <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/products">About Us</Link>
          </li>
          <li>
            <Link to="/detail">Contact Us</Link>
          </li> */}
        </ul>
        <Routes>
          <Route exact path="/" element={<ProductList />}></Route>
          <Route exact path="/products" element={<ProductList />}></Route>
          <Route exact path="/detail" element={<ProductDetail />}></Route>
        </Routes>
      </div>{" "}
    </Router>
  );
}

export default App;
